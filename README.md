# B2B Lead Solution
This is an attempt at a B2B Lead Solution. Currently, Salesforce leads are only designed to market to and qualify leads as individuals, not considered together as a business. This solution helps group together leads in a 'Business' container, much the same way Contact records are grouped together under a Parent 'Account' record on Salesforce.

## Prerequisites

## Installation

## Usage

## Components

## Development Notes
- Requires CPX Framework Installation
