/**
* @description				This methods provides executable logic against collections of Business__c reocrds
* @author					Cloud Performer Inc. <emailaddress>
* @version					1.0
* @history					2021-06-17 - Created
**/
public with sharing class BusinessMethods {
    public static void ConvertLeadsRelatedToBusiness(List<Business__c> businessRecordList) {
        //make sure each business passed in has the right settings
        Map<Id,Business__c> businessMap = new Map<Id,Business__c>();
        for(Business__c b:businessRecordList) {
            if(b.ConvertLeads__c && b.ConvertedAccountId__c != null && b.ConvertedOpportunityId__c != null){
                businessMap.put(b.Id,b);
            }
        }
        businessMap = new Map<Id,Business__c>([SELECT Id, Name, ConvertedAccountId__c, ConvertedOpportunityId__c, ConvertLeads__c,
            ConvertedAccountId__r.OwnerId FROM Business__c WHERE Id IN :businessMap.keySet()]);
        List<Database.LeadConvert> leadsToConvert = new List<Database.LeadConvert>();
        try {
            for(Lead l:[SELECT Id, ParentBusinessId__c FROM Lead WHERE ParentBusinessId__c IN :businessMap.keySet() AND 
                    IsSyncedWithRelatedBusiness__c = true AND IsConverted = false]) {
                Database.LeadConvert lConvertInfo = new Database.LeadConvert();
                Business__c b = businessMap.get(l.ParentBusinessId__c);
                lConvertInfo.setLeadId(l.Id);
                lConvertInfo.setAccountId(b.ConvertedAccountId__c);
                lConvertInfo.setOwnerId(b.ConvertedAccountId__r.OwnerId);
                lConvertInfo.setConvertedStatus('Closed - Converted');
                lConvertInfo.setDoNotCreateOpportunity(true);
                leadsToConvert.add(lConvertInfo);
            }
        } catch(System.Exception ex) {
            cpx.Log.Error(ex,'BusinessMethods');
            throw ex;
        }
        if(!leadsToConvert.isEmpty()) {
            List<Database.LeadConvertResult> conversionResultList = Database.convertLead(leadsToConvert, false);
            List<OpportunityContactRole> contactRolesToInsert = CreateOpportunityContactRolesForConvertedLeads(conversionResultList, 
                businessMap);
            try {
                if(!contactRolesToInsert.isEmpty()){
                    cpx.Log.Debug('Contact Roles To Insert.',JSON.serializePretty(contactRolesToInsert),'BusinessMethods');
                    insert contactRolesToInsert;
                } else {
                    cpx.Log.Error('No Contact Roles to Insert','There were no opportunity Contact Roles created by CreateOpportunityContactRolesForConvertedLeads.','BusinessMethods');
                }
            } catch(System.Exception ex) {
                cpx.Log.Error(ex,'BusinessMethods');
            }
            List<String> errMessArray = new List<String>();
            for(Database.LeadConvertResult result:conversionResultList){
                if(!result.isSuccess()){
                    for(Database.Error err:result.getErrors()){
                        errMessArray.add(err.getMessage());
                    }
                }
            }
            if(!errMessArray.isEmpty()) {
                cpx.Log.Error('Error Converting Leads',String.join(errMessArray,'; '),'BusinessMethods');
            }
        }
        //finally turn off convert leads functionality for those businesses
        cpx.Flow.SuppressTrigger('BusinessTrigger');
        for(Business__c b:businessMap.values()){
            b.ConvertLeads__c = false;
        }
        if(!businessMap.isEmpty())
        {
            try {
                update businessMap.values();
            } catch(System.Exception ex) {
                cpx.Log.Error(ex,'BusinessMethods');
                throw ex;
            }
        }
    }
    public static void FireBusinessUpdateEvents(List<Business__c> newBusinesses){
        List<BusinessConvertUpdate__e> eventRecords = new List<BusinessConvertUpdate__e>();
        for(Business__c b:newBusinesses) {
            BusinessConvertUpdate__e eventRecord = new BusinessConvertUpdate__e(
                BusinessDetails__c = JSON.serialize(b));
            eventRecords.add(eventRecord);
        }
        if(!eventRecords.isEmpty()) {
            EventBus.publish(eventRecords);
        }
    }
    private static List<OpportunityContactRole> CreateOpportunityContactRolesForConvertedLeads(List<Database.LeadConvertResult> resultList,
            Map<Id,Business__c> businessMap) {
        Set<Id> leadIdSet = new Set<Id>();
        List<OpportunityContactRole> oppContactRolesToCreate = new List<OpportunityContactRole>();
        for(Database.LeadConvertResult result:resultList){
            if(result.isSuccess()){
                leadIdSet.add(result.getLeadId());
            }
        }
        Map<Id,Lead> leadMap = new Map<Id,Lead>([SELECT Id, ParentBusinessId__c FROM Lead WHERE Id IN :leadIdSet]);
        cpx.Log.Debug('Leads Found:','Leads Found from set: ' + JSON.serialize(leadIdSet) + ': ' + JSON.serializePretty(leadMap,false),'BusinessMethods');
        
        for(Database.LeadConvertResult result:resultList) {
            if(result.isSuccess()){
                Lead l = leadMap.get(result.getLeadId());
                Business__c b = businessMap.get(l.ParentBusinessId__c);
                Id oppId = b.ConvertedOpportunityId__c;
                oppContactRolesToCreate.add(new OpportunityContactRole(ContactId=result.getContactId(),OpportunityId=oppId));
            }
        }
        return oppContactRolesToCreate;
    }
}
