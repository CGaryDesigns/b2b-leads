public with sharing class BusinessTriggerHandler extends cpx.Framework.TriggerHandlerBase {
    public override void isAfterUpdate(Map<Id,SObject> newMap, Map<Id,SObject> oldMap) {
        cpx.Flow.SuppressTrigger('BusinessTrigger.isAfterUpdate');
        BusinessMethods.FireBusinessUpdateEvents((List<Business__c>)newMap.values());
    }
}
