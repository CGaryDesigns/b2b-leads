/**
* @description				The purpose of this class is to expose methods that can provide logic to user interfaces
*                           that focus on functionality in dealing with Business__c records.
* @author					Cloud Performer Inc. <cgary@cloudperformer.com>
* @version					1.0
* @history					2021-06-16 - Created
**/
public with sharing class BusinessUIMethods {
    /** Excpetion class */
    public class UIException extends System.Exception {}

    @AuraEnabled(cacheable=true)
    public static List<Object> ObtainFieldsetFields(String fieldsetName) {
        try {
            Map<String, Schema.FieldSet> businessFieldSetMap = Business__c.SObjectType.getDescribe().fieldSets.getMap();
            if(!businessFieldSetMap.containsKey(fieldsetName)){
                throw new BusinessUIMethods.UIException('No Fieldset named \'' + fieldsetName + '\' on Business object.');
            }
            return businessFieldSetMap.get(fieldsetName).getFields();
        } catch(System.Exception ex) {
            cpx.Log.Error(ex,'BusinessUIMethods');
            throw new AuraHandledException(ex.getMessage());
        }
    }
    @AuraEnabled(cacheable=true)
    public static List<Business__c> ObtainRelatedBusinessesById(String sourceBusinessId) {
        Business__c businessPassedIn;
        List<Business__c> businessList = new List<Business__c>();
        Id topOfHierarchyId;
        //accomodate for Lead record ID's being passed in.
        if(sourceBusinessId.startsWith('00Q')){
            sourceBusinessId = [SELECT Id, ParentBusinessId__c FROM Lead WHERE Id=:sourceBusinessId].ParentBusinessId__c;
        }
        topOfHierarchyId = sourceBusinessId;
        try {
            businessPassedIn = [SELECT Id, Name, BusinessId__c, ConvertLeads__c, ConvertedAccountId__c,
                ConvertedOpportunityId__c, CreatedById, LastModifiedById, OwnerId, ParentBusinessId__c, Website__c FROM
                Business__c WHERE Id = :sourceBusinessId];
            if(businessPassedIn.ParentBusinessId__c != null){
                topOfHierarchyId = businessPassedIn.ParentBusinessId__c;
            }
            businessList = [SELECT Id, Name, BusinessId__c, ConvertLeads__c, ConvertedAccountId__c,
                ConvertedOpportunityId__c, CreatedById, LastModifiedById, OwnerId, ParentBusinessId__c, Website__c,
                (SELECT Id, Name, BusinessId__c, ConvertLeads__c, ConvertedAccountId__c, ConvertedOpportunityId__c,
                CreatedById, LastModifiedById, OwnerId, ParentBusinessId__c, Website__c FROM RelatedBusinesses__r)
                FROM Business__c WHERE Id = :topOfHierarchyId];
        } catch(System.Exception ex) {
            cpx.Log.Error(ex,'BusinessUIMethods');
            throw new AuraHandledException(ex.getMessage());
        }
        return businessList;
    }
    @AuraEnabled
    public static List<Business__c> ObtainBusinessesByParentId(String parentBusinessId){
        List<Business__c> businessList;
        try {
            businessList = [SELECT Id, Name, BusinessId__c, ConvertLeads__c, ConvertedAccountId__c, ConvertedOpportunityId__c,
                CreatedById, LastModifiedById, OwnerId, ParentBusinessId__c, Website__c FROM Business__c WHERE 
                ParentBusinessId__c = :parentBusinessId];
        } catch(System.Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }

        return businessList;
    }
}
