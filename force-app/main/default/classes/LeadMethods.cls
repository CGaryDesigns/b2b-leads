/**
* @description				The purpose of this class is to expose logic that can be used based on Collections of Leads
*                           Usually these collections of leads are passed in from Data Manipulation operations. All methods
*                           in this class should be static.
* @author					Cloud Performer Inc. <cgary@cloudperformer.com>
* @version					1.0
* @history					2021-06-16 - Created
**/
public with sharing class LeadMethods {
    /**
     * This method will update the CompanyName field on Leads if the Lead Related to a Parent Business and is 
     * set up to be Synced.
     * 
     * @param   newLeadList     List<Lead> the list of new Leads
     */
    public static void UpdateCompanyNameOfLead(List<Lead> newLeadList) {
        Set<Id> businessIdSet = new Set<Id>();
        //get the Businesses of qualified Leads
        for(Lead l:newLeadList) {
            if(l.ParentBusinessId__c != null && l.IsSyncedWithRelatedBusiness__c) {
                businessIdSet.add(l.ParentBusinessId__c);
            }
        }
        Map<Id,Business__c> businessMap = new Map<Id,Business__c>([SELECT Id, Name FROM Business__c 
            WHERE Id IN :businessIdSet]);
        for(Lead l:newLeadList){
            if(l.ParentBusinessId__c != null && l.IsSyncedWithRelatedBusiness__c && 
                    businessMap.containsKey(l.ParentBusinessId__c)) {
                l.Company = businessMap.get(l.ParentBusinessId__c).Name;
            }
        }
    }
    /**
     * This method will update the Business Records related to newly converted Leads with the Account ID, Opportunity ID
     * (if it exists) and flip the ConvertLeads flag to true.
     * 
     * @param   newLeadMap      Map<Id,Lead> the map of new Lead data by Lead Id
     * @param   oldLeadMap     Map<Id,Lead> the map of old Lead data by Lead Id
     */
    public static void UpdateRelatedBusinessesOfConvertedLeads(Map<Id,Lead> newLeadMap, Map<Id,Lead> oldLeadMap) {
        //exclude leads that are not newly converted or do not have the IsSyncedWIthRelatedBusiness__c set to true
        Set<Id> businessIdSet = new Set<Id>();
        Map<Id,Lead> qualifiedLeads = new Map<Id,Lead>();
        for(Lead l:newLeadMap.values()) {
            Lead oldLead = oldLeadMap.get(l.Id);
            if(oldLead != null && oldLead.IsConverted == false && l.IsSyncedWithRelatedBusiness__c && l.IsConverted) {
                qualifiedLeads.put(l.Id,l);
                businessIdSet.add(l.ParentBusinessId__c);
            }
        }
        //get the Businesses related to the Qualified Leads
        Map<Id,Business__c> businessMapToUpdate = new Map<Id,Business__c>([SELECT Id, ConvertedAccountId__c, 
            ConvertedOpportunityId__c, ConvertLeads__c FROM Business__c WHERE Id IN :businessIdSet]);
        //as we loop through the new Leads, get the Parent Business, and update it with relavent information
        for(Lead l:qualifiedLeads.values()) {
            if(businessMapToUpdate.containsKey(l.ParentBusinessId__c)){
                Business__c b = businessMapToUpdate.get(l.ParentBusinessId__c);
                b.ConvertedAccountId__c = l.ConvertedAccountId;
                b.ConvertedOpportunityId__c = l.ConvertedOpportunityId;
                b.ConvertLeads__c = true;
            }
        }
        if(!businessMapToUpdate.isEmpty()) {
            update businessMapToUpdate.values();
        }
    }
    /**
     * This method is responsible for creating New BusinessLeadRole records whenever the ParentBusinessId__c on a 
     * Lead record changes. If the ParentBusinessId__c field is newly populated, it creates a new BusinessLeadRole__c recorrd.
     * If the ParentBusinessId__c field is updated, it will Remove the create BusinessLeadRole Records that matches the 
     * old Business/Lead Id combo and add the new record.  If the ParentBusinessId__c field is emptied, it will simply remove
     * the BusinessLeadRole record that matches the old Business/Lead Id combo.
     * 
     * @param   newLeadMap      Map<Id,Lead> - new Lead Data
     * @param   oldLeadMap      Map<Id,Lead> - old Lead Data
     */
    public static void ManageLeadRolesbyLead(Map<Id,Lead> newLeadMap, Map<Id,Lead> oldLeadMap){
        List<BusinessLeadRole__c> leadRolesToCreate = new List<BusinessLeadRole__c>();
        List<BusinessLeadRole__c> leadRolesToRemove = new List<BusinessLeadRole__c>();
        Map<String,BusinessLeadRole__c> currentBusinessLeadRolesByKey = new Map<String,BusinessLeadRole__c>();
        //create new Lead Roles
        try {
            for(Lead l:newLeadMap.values()){
                Lead oldLead = oldLeadMap.get(l.Id);
                if(((oldLead == null) || (l.ParentBusinessId__c != oldLead.ParentBusinessId__c)) && l.ParentBusinessId__c != null ) {
                    leadRolesToCreate.add(new BusinessLeadRole__c(Name = l.FirstName + ' ' + l.LastName, Lead__c = l.Id, 
                        Business__c = l.ParentBusinessId__c));
                }
            }
            //get current BusinessLeadRole__c records and key them, then find the BusinessLeadRole__c records to delete
            if(oldLeadMap != null && !oldLeadMap.isEmpty()) {
                for(BusinessLeadRole__c blr:[SELECT Id, Lead__c, Business__c FROM BusinessLeadRole__c WHERE Lead__c IN :oldLeadMap.keySet()]) {
                    currentBusinessLeadRolesByKey.put(blr.Lead__c + '|' + blr.Business__c,blr);
                }
                for(Lead l:oldLeadMap.values()){
                    if(l.ParentBusinessId__c != null) {
                        String key = l.Id + '|' + l.ParentBusinessId__c;
                        if(currentBusinessLeadRolesByKey.containsKey(key)){
                            leadRolesToRemove.add(currentBusinessLeadRolesByKey.get(key));
                        }
                    }
                }
            }
        } catch(System.Exception ex) {
            cpx.Log.Error(ex,'LeadMethods');
            throw ex;
        }
        try {
            if(!leadRolesToCreate.isEmpty()) {
                insert leadRolesToCreate;
            }
            if(!leadRolesToRemove.isEmpty()) {
                delete leadRolesToRemove;
            }
        } catch(System.Exception ex) {
            cpx.Log.Error(ex,'LeadMethods');
            throw ex;
        }

    }
}
