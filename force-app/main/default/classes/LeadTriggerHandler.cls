public with sharing class LeadTriggerHandler extends cpx.Framework.TriggerHandlerBase {
    public override void isBeforeInsert(List<SObject> newList) {
        cpx.Flow.SuppressTrigger('LeadTrigger.isBeforeInsert');
        LeadMethods.UpdateCompanyNameOfLead((List<Lead>)newList);
    }
    public override void isAfterInsert(Map<Id,SObject> newMap) {
        cpx.Flow.SuppressTrigger('LeadTrigger.isAfterInsert');
        LeadMethods.ManageLeadRolesbyLead((Map<Id,Lead>)newMap, new Map<Id,Lead>());
    }
    public override void isBeforeUpdate(Map<Id,SObject> newMap, Map<Id,SObject> oldMap) {
        cpx.Flow.SuppressTrigger('LeadTrigger.isBeforeUpdate');
        LeadMethods.UpdateCompanyNameOfLead((List<Lead>)newMap.values());
    }
    public override void isAfterUpdate(Map<Id,SObject> newMap, Map<Id,SObject> oldMap) {
        cpx.Flow.SuppressTrigger('LeadTrigger.isAfterUpdate');
        LeadMethods.ManageLeadRolesbyLead((Map<Id,Lead>)newMap, (Map<Id,Lead>)oldMap);
        LeadMethods.UpdateRelatedBusinessesOfConvertedLeads((Map<Id,Lead>)newMap, (Map<Id,Lead>)oldMap);
    }
}
