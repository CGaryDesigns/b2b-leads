trigger BusinessTrigger on Business__c (before insert, before update, before delete, after insert, after update,
        after undelete) {
    cpx.Framework.HandleTrigger('BusinessTrigger', new BusinessTriggerHandler());
}