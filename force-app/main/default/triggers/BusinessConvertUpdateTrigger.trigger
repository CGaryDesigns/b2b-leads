trigger BusinessConvertUpdateTrigger on BusinessConvertUpdate__e (after insert) {
    //rehydrate the Business Record that comes with the Trigger
    List<Business__c> businessList = new List<Business__c>();
    try {
        for(BusinessConvertUpdate__e item:Trigger.new) {
            if(item.BusinessDetails__c != null){
                Business__c b = (Business__c)JSON.deserialize(item.BusinessDetails__c, Business__c.class);
                businessList.add(b);
            }
        }
    } catch(System.Exception ex) {
        cpx.Log.Error(ex,'BusinessConverUpdateTrigger');
        throw ex;
    }
    //now business records are rehydrated, attempt to convert leads related to them
    BusinessMethods.ConvertLeadsRelatedToBusiness(businessList);

}