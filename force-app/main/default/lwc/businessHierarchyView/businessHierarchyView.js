import { LightningElement, api, track, wire } from 'lwc';
import ObtainRelatedBusinessesById from '@salesforce/apex/BusinessUIMethods.ObtainRelatedBusinessesById';
import ObtainBusinessesByParentId from '@salesforce/apex/BusinessUIMethods.ObtainBusinessesByParentId';


export default class BusinessHierarchyView extends LightningElement {
    @api recordId;
    @track businessRecords = [];
    @track expandedRows = [];
    columns = [
        {fieldName:'Name',label:'Business Name',type:'url',typeAttributes:{label:{fieldName:'Label'}}},
        {fieldName:'BusinessId__c',label:'Business ID',type:'String'},
        {fieldName:'Website__c',label:'Website',type:'url'}
    ]
    isLoading = true;

    @wire(ObtainRelatedBusinessesById,{sourceBusinessId:'$recordId'})
    ProcessBusinessHierarchy({err, data}) {
        var tempBusinessRecords = [];
        var tempExpandedRows = [];
        if(err) {
            window.console.error(err);
        }
        if(data) {
            data.forEach((element)=>{
                let {Id, Name, BusinessId__c, Website__c, RelatedBusinesses__r } = element;
                let newObj = {'Id':Id, 'Name': '/' + Id, Label: Name, 'BusinessId__c':BusinessId__c,'Website__c':Website__c};
                if(newObj.Id == this.recordId){
                    newObj.Label += ' (current record)';
                }
                var childBusiness = [];
                if(RelatedBusinesses__r) {
                    element.RelatedBusinesses__r.forEach(item => {
                        let clonedItem = {};
                        Object.assign(clonedItem,item);
                        clonedItem.Label = clonedItem.Name;
                        if(clonedItem.Id == this.recordId) {
                            clonedItem.Label += ' (current record)';
                        }
                        clonedItem.Name = '/' + clonedItem.Id;
                        clonedItem._children = [];
                        childBusiness.push(clonedItem);
                    })
                    newObj._children = childBusiness;
                }
                tempBusinessRecords.push(newObj);
                tempExpandedRows.push(Id);
            });
        }
        this.businessRecords = tempBusinessRecords;
        this.expandedRows = tempExpandedRows;
        this.isLoading = false;
        window.console.log(JSON.stringify(this.businessRecords,null,"\t"));
    }

    handleToggle(event){
        window.console.log(JSON.stringify(event.detail,null,"\t"));
        if(event.detail.isExpanded){
            let tempExpandedRows = this.expandedRows;
            tempExpandedRows.push(event.detail.name);
            this.expandedRows = tempExpandedRows;
        }
        ObtainBusinessesByParentId({'parentBusinessId':event.detail.name}).then((result)=>{
            let newBusinessRecords = this.reformatBusinessRecords(result);
            this.businessRecords = this.getNewDataWithChildren(event.detail.name, this.businessRecords, newBusinessRecords);
        }).catch(err=>{
            window.console.error(err);
        })
    }

    reformatBusinessRecords(data){
        var tempBusinessRecords = [];
        var tempExpandedRows = [];
        if(data) {
            data.forEach((element)=>{
                let {Id, Name, BusinessId__c, Website__c, RelatedBusinesses__r } = element;
                let newObj = {'Id':Id, 'Name': '/' + Id, Label: Name, 'BusinessId__c':BusinessId__c,'Website__c':Website__c};
                if(newObj.Id == this.recordId){
                    newObj.Label += ' (current record)';
                }
                var childBusiness = [];
                if(RelatedBusinesses__r) {
                    element.RelatedBusinesses__r.forEach(item => {
                        let clonedItem = {};
                        Object.assign(clonedItem,item);
                        clonedItem.Label = clonedItem.Name;
                        if(clonedItem.Id == this.recordId) {
                            clonedItem.Label += ' (current record)';
                        }
                        clonedItem.Name = '/' + clonedItem.Id;
                        clonedItem._children = [];
                        childBusiness.push(clonedItem);
                    });
                    
                }
                newObj._children = childBusiness;
                tempBusinessRecords.push(newObj);
                tempExpandedRows.push(Id);
            });
        }
        return tempBusinessRecords;
    }

    getNewDataWithChildren(rowName, data, children) {
        return data.map((row)=>{
            let hasChildrenContent = false;
            if(row.hasOwnProperty('_children') && Array.isArray(row._children) && row._children.length > 0){
                hasChildrenContent = true;
            }
            if(row.Id === rowName) {
                row._children = children;
            } else if(hasChildrenContent) {
                this.getNewDataWithChildren(rowName,row._children,children);
            }
            return row;
        });
    }

    get showHierarchy() {
        if(this.businessRecords.length > 0) {
            return true;
        }
        return false;
    }

}